#!/bin/bash

java -Xmx512m -cp "$(cd `dirname "$0"` && pwd)/innova-echo-server-assembly-0.0.1-SNAPSHOT.jar" ru.kulikovd.innova.echo.InitiatorApp --proxy-addr localhost --proxy-port 11111 --senders 3  --receivers 3
