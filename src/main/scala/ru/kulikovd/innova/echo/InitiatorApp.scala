package ru.kulikovd.innova.echo

import java.net.InetSocketAddress

import akka.actor._
import akka.io.{Tcp, IO}
import akka.routing.RoundRobinPool
import akka.util.ByteString


case class Config(proxyAddr: String = "localhost", proxyPort: Int = 11111, senderThreads: Int = 1, receiverThreads: Int = 1)

object Config {
  val parser = new scopt.OptionParser[Config]("echo-server") {
    opt[String]('a', "proxy-addr") action { (addr, c) ⇒ c.copy(proxyAddr = addr) } text "Proxy server ip address"
    opt[Int]('p', "proxy-port") action { (port, c) ⇒ c.copy(proxyPort = port) } text "Proxy server port"
    opt[Int]('s', "senders") action { (s, c) ⇒ c.copy(senderThreads = s) } text "Number of sender threads"
    opt[Int]('r', "receivers") action { (r, c) ⇒ c.copy(receiverThreads = r) } text "Number of receiver threads"
  }
}


object InitiatorApp extends App {
  val system = ActorSystem("initiator-system")

  val config = Config.parser.parse(args, Config()).getOrElse(throw new Exception("Can not parse arguments!"))

  val sendWriter    = system.actorOf(Props(new FileWriter("initiator_send.txt")))
  val receiveWriter = system.actorOf(Props(new FileWriter("initiator_receive.txt")))

  val manager = system.actorOf(Props(new CommandManager), "command-manager")

  val initiator = system.actorOf(
    Props(new InitiatorService(config, manager, sendWriter, receiveWriter))
      .withRouter(RoundRobinPool(nrOfInstances = config.senderThreads)), "tcp-echo-service")

  manager ! initiator

  println("Welcome to echo-server!\nAvailable commands: start, stop, exit")

  while (true) {
    readLine("\nEnter command: ") match {
      case "start" ⇒
        manager ! 'Start

      case "stop" ⇒
        manager ! 'Stop

      case "exit" ⇒
        manager ! 'Exit
        readLine("exit...")

      case other if other.trim.nonEmpty ⇒
        println(s"Unexpected command: '$other'!")

      case _ ⇒ // skip
    }
  }
}


class CommandManager extends Actor {
  var number = 0l
  var received = 0l

  def receive = {
    case initiator: ActorRef ⇒
      context.become(suspended(initiator))
  }

  def suspended(initiator: ActorRef): Receive = {
    case 'Start ⇒
      initiator ! NotifyProxy("Initiator Server is started")

      0 to 50 foreach { _ ⇒
        initiator ! Send(number)
        number += 1
      }

      context.become(started(initiator))

    case 'Exit ⇒
      context.become(exited(initiator))
      self ! 'Check

    case _: Ack ⇒
      received += 1
  }

  def started(initiator: ActorRef): Receive = {
    case 'Stop ⇒
      context.become(suspended(initiator))
      initiator ! NotifyProxy("Initiator Server is suspended")

    case 'Exit ⇒
      context.become(exited(initiator))
      self ! 'Check

    case _: Ack ⇒
      initiator ! Send(number)
      number += 1
      received += 1
  }

  def exited(initiator: ActorRef): Receive = {
    case Ack(n) ⇒
      received += 1
      self ! 'Check

    case 'Check ⇒
      if (received == number) {
        initiator ! NotifyProxy("Initiator Server has exited")

        Thread.sleep(1200)
        System.exit(0)
      }
  }
}


case class Send(n: Long)
case class NotifyProxy(msg: String)
case class Ack(n: Long) extends Tcp.Event


class InitiatorService(config: Config, manager: ActorRef, sendWriter: ActorRef, receiveWriter: ActorRef) extends Actor with ActorLogging with Stash {
  import context.system

  val EOL = ByteString("|")

  IO(Tcp) ! Tcp.Connect(new InetSocketAddress(config.proxyAddr, config.proxyPort))

  def receive = {
    case _: Tcp.Connected ⇒
      sender ! Tcp.Register(self)
      context.become(connected(sender))
      unstashAll()

    case _ ⇒ stash()
  }

  def connected(proxyConnection: ActorRef): Receive = {
    case Send(n) ⇒
      log.debug("Send to proxy '{}'", n)

      sendWriter ! Write(n.toLong)
      proxyConnection ! Tcp.Write(ByteString(n.toString) ++ EOL)

    case NotifyProxy(msg) ⇒
      proxyConnection ! Tcp.Write(ByteString("NTF: " + msg) ++ EOL)

    case Tcp.Received(data) ⇒
      val text = data.utf8String.trim

      text.split('|') foreach { n ⇒
        log.debug("From proxy '{}'", n)

        try {
          receiveWriter ! Write(n.toLong)
          manager ! Ack(n.toLong)
        } catch {
          case e: NumberFormatException ⇒ // skip. todo: correctly receive chunked data
        }
      }
  }
}
