package ru.kulikovd.innova.echo

import java.net.InetSocketAddress

import akka.actor._
import akka.io.{Tcp, IO}
import akka.util.ByteString


case class RegisterClient(initializerConn: ActorRef)


object ProxyApp extends App {
  val system = ActorSystem("initiator-system")

  val echoService = system.actorOf(Props(new EchoServiceClient))

  system.actorOf(Props(classOf[ProxyService], echoService), "tcp-proxy-service")
}


/**
 * Wait while initializer and echo server connecting and communicate between them
 */
class ProxyService(echoService: ActorRef) extends Actor with ActorLogging {
  import context.system

  IO(Tcp) ! Tcp.Bind(self, new InetSocketAddress("localhost", 11111))

  def receive = {
    case Tcp.Connected(remote, _) ⇒
      log.info("Remote address {} connected", remote)

      echoService ! RegisterClient(sender)
      sender ! Tcp.Register(system.actorOf(Props(new ProxyConnectionHandler(echoService))))

    case Tcp.Bound(local) ⇒
      log.info("Proxy successfully bound to {}", local)
  }
}


class ProxyConnectionHandler(echoService: ActorRef) extends Actor with ActorLogging {

  def receive = {
    case Tcp.Received(data) ⇒
      data.utf8String.trim.split('|') foreach {
        case msg if msg.startsWith("NTF: ") ⇒ log.info(msg.drop(5))
        case msg ⇒ echoService ! ByteString(msg)
      }

    case _: Tcp.ConnectionClosed | _: Terminated ⇒
      log.info("Stopping, because connection for remote address closed")
      context.stop(self)
  }
}


/**
 * Await connection from echo server application
 */
class EchoServiceClient extends Actor with ActorLogging {
  import context.system

  IO(Tcp) ! Tcp.Bind(self, new InetSocketAddress("localhost", 11112))

  val EOL = ByteString("|")

  def receive = {
    case c @ Tcp.Connected(remote, local) ⇒
      sender ! Tcp.Register(self)
      context.become(idle(sender))
  }

  /**
   * When echo server connected — wait initializer connections
   */
  def idle(echoConnection: ActorRef): Receive = {
    case RegisterClient(initializerConn) ⇒
      log.debug("Register initializer {}", initializerConn)
      context.become(connected(echoConnection, initializerConn))
  }


  def connected(echoConnection: ActorRef, initializerConn: ActorRef): Receive = idle(echoConnection: ActorRef) orElse {
    case data: ByteString ⇒
      log.debug("Send to echo server '{}'", data.utf8String.trim)
      echoConnection ! Tcp.Write(data ++ EOL)

    case Tcp.Received(data) ⇒
      log.debug("Response from echo '{}'", data.utf8String.trim)
      initializerConn ! Tcp.Write(data)

    case oth => log.warning("{}", oth)
  }
}
