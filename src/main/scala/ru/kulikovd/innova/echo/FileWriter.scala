package ru.kulikovd.innova.echo

import java.io.{FileOutputStream, OutputStreamWriter, BufferedWriter}
import scala.concurrent.duration._

import akka.actor.{ActorLogging, Actor}


case class Write(n: Long)
case object Flush


class FileWriter(filename: String) extends Actor with ActorLogging {
  import context.dispatcher

  val fileWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename)))

  val buffer        = collection.mutable.TreeSet.empty[Long]
  val MaxBufferSize = 10000
  var last          = -1l

  context.system.scheduler.schedule(1 seconds, 1 seconds, self, Flush)

  def receive = {
    case Write(n) ⇒
      buffer += n

      if (buffer.size > MaxBufferSize) {
        self ! Flush
      }

    case Flush ⇒
      0 until buffer.size foreach { _ ⇒
        if (buffer.nonEmpty) {
          val n = buffer.head
          if (last + 1 == n) {
            last = n
            buffer -= n

            fileWriter.write(n.toString)
            fileWriter.newLine()
          }
        }
      }

      fileWriter.flush()
  }

  override def postStop() {
    fileWriter.close()
  }
}
