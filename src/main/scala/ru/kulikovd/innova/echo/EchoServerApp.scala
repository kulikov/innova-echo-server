package ru.kulikovd.innova.echo

import java.net.InetSocketAddress

import akka.actor._
import akka.io.{Tcp, IO}
import akka.util.ByteString


object EchoServerApp extends App {
  val system = ActorSystem("echo-server-system")

  val config     = Config.parser.parse(args, Config(proxyPort = 11112)).getOrElse(throw new Exception("Can not parse arguments!"))
  val echoWriter = system.actorOf(Props(new FileWriter("echo.txt")))

  system.actorOf(Props(new EchoServer(config, echoWriter)), "tcp-echo-server")
}


class EchoServer(config: Config, echoWriter: ActorRef) extends Actor with ActorLogging {
  import context.system

  IO(Tcp) ! Tcp.Connect(new InetSocketAddress(config.proxyAddr, config.proxyPort))

  val EOL = ByteString("|")

  def receive = {
    case Tcp.Connected(remote, _) ⇒
      log.info("Remote address {} connected", remote)
      sender ! Tcp.Register(self)
      context.become(connected)
  }

  def connected: Receive = {
    case Tcp.Received(data) ⇒
      val text = data.utf8String.trim
      log.debug("Received '{}'", text)

      text.split('|') foreach { n ⇒
        log.debug("Echo '{}'", n)

        try {
          echoWriter ! Write(n.toLong)
          sender ! Tcp.Write(ByteString(n) ++ EOL)
        } catch {
          case e: NumberFormatException ⇒ // skip. todo: correctly receive chunked data
        }
      }

    case oth => log.warning("{}", oth)
  }
}
